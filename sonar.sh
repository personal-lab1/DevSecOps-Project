#!/bin/bash

sudo apt-update
# Download sonar-scanner
wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.6.2.2472-linux.zip

# Unzip the downloaded file
unzip sonar-scanner-cli-4.6.2.2472-linux.zip

# Move sonar-scanner to the /opt directory
sudo mv sonar-scanner-4.6.2.2472-linux /opt/sonar-scanner

# Add sonar-scanner to the PATH
echo 'export PATH="$PATH:/opt/sonar-scanner/bin"' >> ~/.bashrc

# Reload the .bashrc file
source ~/.bashrc
